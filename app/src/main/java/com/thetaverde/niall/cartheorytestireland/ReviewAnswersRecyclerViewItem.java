package com.thetaverde.niall.cartheorytestireland;

public class ReviewAnswersRecyclerViewItem {


    private QuestionObject questionObject;

    private String Question;
    private String CorrectAnswer;
    private Boolean questionAnsweredCorrectly;
    private String wrongAnswerThatWasSelected;


    public ReviewAnswersRecyclerViewItem(QuestionObject questionObject){

        this.setQuestion(questionObject.getQuestion());

        this.setCorrectAnswer(questionObject.getCorrectAnswer());

        this.setQuestionAnsweredCorrectly(questionObject.getQuestionAnsweredCorrectly());

        this.setWrongAnswerThatWasSelected(questionObject.getWrongAnswerThatWasSelected());



    }




    public QuestionObject getQuestionObject() {
        return questionObject;
    }

    public void setQuestionObject(QuestionObject questionObject) {
        this.questionObject = questionObject;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public Boolean getQuestionAnsweredCorrectly() {
        return questionAnsweredCorrectly;
    }

    public void setQuestionAnsweredCorrectly(Boolean questionAnsweredCorrectly) {
        this.questionAnsweredCorrectly = questionAnsweredCorrectly;
    }

    public String getWrongAnswerThatWasSelected() {
        return wrongAnswerThatWasSelected;
    }

    public void setWrongAnswerThatWasSelected(String wrongAnswerThatWasSelected) {
        this.wrongAnswerThatWasSelected = wrongAnswerThatWasSelected;
    }
}
