package com.thetaverde.niall.cartheorytestireland;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ReviewAnswersRecyclerViewAdapter extends RecyclerView.Adapter<ReviewAnswersRecyclerViewAdapter.MyViewHolder>  {


    ArrayList<ReviewAnswersRecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();

    Context context;


    ReviewAnswersRecyclerViewAdapter(ArrayList<ReviewAnswersRecyclerViewItem> recyclerViewItemsArrayList, Context context){

        this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;

        this.context = context;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_answer_recycler_view_row,parent,false);

        return new MyViewHolder(view,context,recyclerViewItemsArrayList);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.reviewAnswersRV_Question.setText("Q" +  String.valueOf(position + 1) + ". " + recyclerViewItemsArrayList.get(position).getQuestion());

        holder.reviewAnswersRV_CorrectAnswer.setText(recyclerViewItemsArrayList.get(position).getCorrectAnswer());


        if(recyclerViewItemsArrayList.get(position).getQuestionAnsweredCorrectly() == null){

            holder.reviewAnswerRV_WrongAnswer.setText("Did not answer!");
            holder.reviewAnswerRV_WrongAnswer.setTextColor(Color.BLACK);
            holder.reviewAnswerRV_WrongAnswer.setBackgroundColor(Color.WHITE); 

        } else if(recyclerViewItemsArrayList.get(position).getQuestionAnsweredCorrectly()){

            holder.reviewAnswerRV_WrongAnswer.setText("CORRECT!");
            holder.reviewAnswerRV_WrongAnswer.setTextColor(Color.BLACK);
            holder.reviewAnswerRV_WrongAnswer.setBackgroundColor(Color.WHITE);


        }else {
            holder.reviewAnswerRV_WrongAnswer.setText(recyclerViewItemsArrayList.get(position).getWrongAnswerThatWasSelected());
            holder.reviewAnswerRV_WrongAnswer.setTextColor(Color.BLACK);
            holder.reviewAnswerRV_WrongAnswer.setBackgroundColor(Color.RED);
        }


        //holder.caveName.setText(recyclerViewItemsArrayList.get(position.getCaveName());
        //holder.caveCountry.setText(recyclerViewItemsArrayList.get(position).getCaveCountry());

    }

    @Override
    public int getItemCount() {
        return recyclerViewItemsArrayList.size();
    }





    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        //TextView caveName;
        //TextView caveCountry;

        TextView reviewAnswersRV_Question;
        TextView reviewAnswersRV_CorrectAnswer;
        TextView reviewAnswerRV_WrongAnswer;

        ArrayList<ReviewAnswersRecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();
        Context context;


        public MyViewHolder(View itemView, Context context, ArrayList<ReviewAnswersRecyclerViewItem> recyclerViewItemsArrayList) {
            super(itemView);

            itemView.setOnClickListener(this);

            this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;
            this.context = context;

            //caveName = (TextView) itemView.findViewById(R.id.caveNameRecyclerViewTextView);
            //caveCountry = (TextView) itemView.findViewById(R.id.caveCountryRecyclerViewTextView);


            reviewAnswersRV_Question = itemView.findViewById(R.id.reviewAnswersRV_Question);

            reviewAnswersRV_CorrectAnswer = itemView.findViewById(R.id.reviewAnswersRV_CorrectAnswer);

            reviewAnswerRV_WrongAnswer = itemView.findViewById(R.id.reviewAnswerRV_WrongAnswer);

        }

        @Override
        public void onClick(View v) {




        }
    }




}
