package com.thetaverde.niall.cartheorytestireland;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;

public class DatabaseHelper extends SQLiteOpenHelper {


    private String DATABASE_PATH = "";

    private static final String DATABASE_NAME = "irish_car_test_db.db";

    //column names
    private static final String section_ColumnName = "Section";
    private static final String question_ColumnName = "Question";
    private static final String correctAnswer_ColumnName = "CorrectAnswer";
    private static final String wrongAnswerOne_ColumnName = "WrongAnswerOne";
    private static final String wrongAnswerTwo_ColumnName = "WrongAnswerTwo";
    private static final String wrongAnswerThree_ColumnName = "WrongAnswerThree";

    private static final String sectionNames_ColumnName = "SectionNames";

    private static final String questionTable = "irishcartest";  //irish car test




    private final Context myContext;

    public SQLiteDatabase db;



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.myContext = context;

        if(android.os.Build.VERSION.SDK_INT >= 17){
            DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DATABASE_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
    }





    public ArrayList<QuestionObject> getQuestions(String sectionName){

        ArrayList<QuestionObject> questions = new ArrayList<>();

        String sqlLevelScoreQuery = "SELECT * FROM " + questionTable + " WHERE " + section_ColumnName + " LIKE '%" + sectionName + "%'";

        //String sqlLevelScoreQuery = "SELECT * FROM " + questionTable;



        Cursor cursor = getReadableDatabase().rawQuery(sqlLevelScoreQuery,null);

        int firstColumnIndex = cursor.getColumnIndex(section_ColumnName);
        int secondColumnIndex = cursor.getColumnIndex(question_ColumnName);
        int thirdColumnIndex = cursor.getColumnIndex(correctAnswer_ColumnName);
        int forthColumnIndex = cursor.getColumnIndex(wrongAnswerOne_ColumnName);
        int fifthColumnIndex = cursor.getColumnIndex(wrongAnswerTwo_ColumnName);
        int sixthColumnIndex = cursor.getColumnIndex(wrongAnswerThree_ColumnName);


        if(cursor.getColumnCount() > 0){

            cursor.moveToFirst();


            while (!cursor.isAfterLast()){

                QuestionObject questionObject = new QuestionObject();

                questionObject.setSection(cursor.getString(firstColumnIndex));
                questionObject.setQuestion(cursor.getString(secondColumnIndex));
                questionObject.setCorrectAnswer(cursor.getString(thirdColumnIndex));
                questionObject.setWrongAnswerOne(cursor.getString(forthColumnIndex));
                questionObject.setWrongAnswerTwo(cursor.getString(fifthColumnIndex));
                questionObject.setWrongAnswerThree(cursor.getString(sixthColumnIndex));


                questions.add(questionObject);

                cursor.moveToNext();

            }

        }


        cursor.close();



        return questions;



    }



    public ArrayList<String> getSectionNames(){

        ArrayList<String> sectionNames = new ArrayList<>();

        String sqlLevelScoreQuery = "SELECT " + sectionNames_ColumnName + " FROM " + questionTable;


        Cursor cursor = getReadableDatabase().rawQuery(sqlLevelScoreQuery,null);

        int firstColumnIndex = cursor.getColumnIndex(sectionNames_ColumnName);



        if(cursor.getColumnCount() > 0){

            cursor.moveToFirst();


            while (!cursor.isAfterLast()){

                sectionNames.add(cursor.getString(firstColumnIndex));

                cursor.moveToNext();

            }

        }


        cursor.close();



        return sectionNames;



    }















    public void createDatabase() {
        createDB();
    }

    private void createDB() {
        boolean dbExist = DBExists();

        if(!dbExist) {
            this.getReadableDatabase();
            copyDBFromResource();
        }
    }

    private boolean DBExists() {
        SQLiteDatabase db = null;

        try {
            String databasePath = DATABASE_PATH + DATABASE_NAME;
            db = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READWRITE);
            db.setLocale(Locale.getDefault());
            db.setLockingEnabled(true);
            db.setVersion(1);
        } catch (SQLiteException e) {
            Log.e("SqlHelper", "database not found");
        }

        if (db != null) {
            db.close();
        }
        return db != null ? true : false;
    }

    private void copyDBFromResource() {
        InputStream inputStream = null;
        OutputStream outStream = null;
        String dbFilePath = DATABASE_PATH + DATABASE_NAME;

        try {
            inputStream = myContext.getAssets().open(DATABASE_NAME);
            outStream = new FileOutputStream(dbFilePath);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }

            outStream.flush();
            outStream.close();
            inputStream.close();


            Log.i("DatabaseCopied","Good Job");

        } catch (IOException e) {
            throw new Error("Problem copying database from resource file.");
        }

    }



























    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }








}
