package com.thetaverde.niall.cartheorytestireland;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

public class SectionsRecyclerViewActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SectionsRecyclerViewAdapter sectionsRecyclerViewAdapter;
    RecyclerView.LayoutManager layoutManager;


    ArrayList<SectionsRecyclerViewItem> recyclerViewArrayList;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.menu.question_activity_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home: // this is for the back arrow on the action bar
                finish();
                return true;

            default:
                return false;
        }

    }







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sections_recycler_view);


        setTitle("Sections");


        //for the back button on the action bar
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }



        recyclerView =  findViewById(R.id.sectionsRecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerViewArrayList = new ArrayList<>();



        //initializing the database helper
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        //getting the section names from the database
        ArrayList<String> sectionNames = databaseHelper.getSectionNames();


        recyclerViewArrayList.clear();

        for (int i = 0; i < sectionNames.size(); i++){

            //there are lots of blank rows in the database for this column because there isn't that many section.
            if(!sectionNames.get(i).equals("")){

            SectionsRecyclerViewItem sectionsRecyclerViewItem = new SectionsRecyclerViewItem();

            sectionsRecyclerViewItem.setSectionName(sectionNames.get(i));

            recyclerViewArrayList.add(sectionsRecyclerViewItem);

            }


        }


        sectionsRecyclerViewAdapter = new SectionsRecyclerViewAdapter(recyclerViewArrayList,this);

        recyclerView.setAdapter(sectionsRecyclerViewAdapter);







    }



    @Override
    protected void onResume() {
        super.onResume();

        sectionsRecyclerViewAdapter = new SectionsRecyclerViewAdapter(recyclerViewArrayList,this);

        recyclerView.setAdapter(sectionsRecyclerViewAdapter);


    }





}
