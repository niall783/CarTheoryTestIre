package com.thetaverde.niall.cartheorytestireland;

public class SectionsRecyclerViewItem {

    private String sectionName;


    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
}
