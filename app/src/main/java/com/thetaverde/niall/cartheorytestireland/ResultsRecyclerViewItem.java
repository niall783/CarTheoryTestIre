package com.thetaverde.niall.cartheorytestireland;

public class ResultsRecyclerViewItem {


    private String percentage;
    private String day;
    private String date;
    private String time;
    private String passOrFail;
    private String testType;

    private String timeToCompleteTest;


    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPassOrFail() {
        return passOrFail;
    }

    public void setPassOrFail(String passOrFail) {
        this.passOrFail = passOrFail;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public String getTimeToCompleteTest() {
        return timeToCompleteTest;
    }

    public void setTimeToCompleteTest(String timeToCompleteTest) {
        this.timeToCompleteTest = timeToCompleteTest;
    }
}
