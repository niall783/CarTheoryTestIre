package com.thetaverde.niall.cartheorytestireland;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ResultsRecyclerViewAdapter extends RecyclerView.Adapter<ResultsRecyclerViewAdapter.MyViewHolder> {





    ArrayList<ResultsRecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();

    Context context;


    ResultsRecyclerViewAdapter(ArrayList<ResultsRecyclerViewItem> recyclerViewItemsArrayList, Context context){

        this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;

        this.context = context;

    }


    @Override
    public ResultsRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.results_recycler_view_row,parent,false);

        return new ResultsRecyclerViewAdapter.MyViewHolder(view,context,recyclerViewItemsArrayList);
    }

    @Override
    public void onBindViewHolder(final ResultsRecyclerViewAdapter.MyViewHolder holder, int position) {

        //If there are results available then populate the recycler view

        if(!MainActivity.sharedPreferences.getString("jsonTestResults", "").equals("")) {






            //date and time
            String day = recyclerViewItemsArrayList.get(position).getDay();
            String date = recyclerViewItemsArrayList.get(position).getDate();
            String time = recyclerViewItemsArrayList.get(position).getTime();

            String percentage = recyclerViewItemsArrayList.get(position).getPercentage();

            holder.dateAndTimeRV_row.setText(day + ", " + date + ", " + time);



            //test type e.g. full test or quick test
            if(recyclerViewItemsArrayList.get(position).getTestType().equals("fullTest")) {

                holder.testTypeRV_row.setText("Full Test");

            }else if (recyclerViewItemsArrayList.get(position).getTestType().equals("quickTest")){

                holder.testTypeRV_row.setText("Quick Test");

            }else{

                holder.testTypeRV_row.setText(recyclerViewItemsArrayList.get(position).getTestType());

            }


            //percentage
            holder.percentageRV_row.setText(percentage);




            //time to complete test
            if(recyclerViewItemsArrayList.get(position).getTestType().equals("fullTest") || recyclerViewItemsArrayList.get(position).getTestType().equals("quickTest")) {
                holder.timeToCompleteTestRV_row.setText(recyclerViewItemsArrayList.get(position).getTimeToCompleteTest());
            }else {
                //there is no need to have a time when it is not a test
                holder.timeToCompleteTestRV_row.setText("");
            }





            //colour the text and insert pass or fail
            if (recyclerViewItemsArrayList.get(position).getPassOrFail().equals("Pass")) {
                Log.i("PassOrFail",recyclerViewItemsArrayList.get(position).getPassOrFail());
                holder.percentageRV_row.setTextColor(Color.GREEN);

                holder.passOrFailRV_row_TV.setText("Pass");
                holder.passOrFailRV_row_TV.setTextColor(Color.GREEN);
            } else {
                holder.percentageRV_row.setTextColor(Color.RED);

                holder.passOrFailRV_row_TV.setText("Fail");
                holder.passOrFailRV_row_TV.setTextColor(Color.RED);
            }

        }else {

            //this displays "There are no results available when they are no results available". it is set in the "ResultsRecyclerViewActivity"
            holder.testTypeRV_row.setText(recyclerViewItemsArrayList.get(position).getTestType());

            Log.i("No Results Available", recyclerViewItemsArrayList.get(position).getTestType());

        }



        //holder.caveName.setText(recyclerViewItemsArrayList.get(position.getCaveName());
        //holder.caveCountry.setText(recyclerViewItemsArrayList.get(position).getCaveCountry());

    }

    @Override
    public int getItemCount() {
        return recyclerViewItemsArrayList.size();
    }





    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        //TextView caveName;
        //TextView caveCountry;

        TextView dateAndTimeRV_row;
        TextView percentageRV_row;
        TextView passOrFailRV_row_TV;
        TextView testTypeRV_row;
        TextView timeToCompleteTestRV_row;


        ArrayList<ResultsRecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();
        Context context;


        public MyViewHolder(View itemView, Context context, ArrayList<ResultsRecyclerViewItem> recyclerViewItemsArrayList) {
            super(itemView);

            itemView.setOnClickListener(this);

            this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;
            this.context = context;

            //caveName = (TextView) itemView.findViewById(R.id.caveNameRecyclerViewTextView);
            //caveCountry = (TextView) itemView.findViewById(R.id.caveCountryRecyclerViewTextView);


            dateAndTimeRV_row = itemView.findViewById(R.id.dateAndTimeRV_row);
            percentageRV_row = itemView.findViewById(R.id.percentageRV_row);

            passOrFailRV_row_TV = itemView.findViewById(R.id.passOrFailRV_row_TV);

            testTypeRV_row = itemView.findViewById(R.id.testTypeRV_row);

            timeToCompleteTestRV_row = itemView.findViewById(R.id.timeToCompleteTestRV_row);

        }

        @Override
        public void onClick(View v) {




        }
    }







}
