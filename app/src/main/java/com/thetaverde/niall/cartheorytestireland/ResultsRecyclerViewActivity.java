package com.thetaverde.niall.cartheorytestireland;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultsRecyclerViewActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    ResultsRecyclerViewAdapter resultsRecyclerViewAdapter;
    RecyclerView.LayoutManager layoutManager;


    ArrayList<ResultsRecyclerViewItem> recyclerViewArrayList;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.menu.question_activity_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home: // this is for the back arrow on the action bar
                finish();
                return true;

            default:
                return false;
        }

    }







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_recycler_view);



        setTitle("Results");


        //for the back button on the action bar
        if(getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }



        recyclerView =  findViewById(R.id.resultsRecyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerViewArrayList = new ArrayList<>();




        //populating the recyclerViewArrayList with the "ResultsRecyclerViewItem" objects
        populateRecyclerViewArrayList();







        resultsRecyclerViewAdapter = new ResultsRecyclerViewAdapter(recyclerViewArrayList,this);

        recyclerView.setAdapter(resultsRecyclerViewAdapter);




    }




    public void  populateRecyclerViewArrayList(){


        if(!MainActivity.sharedPreferences.getString("jsonTestResults", "").equals("")) {

            recyclerViewArrayList.clear();

            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            JSONObject jsonItem = new JSONObject();


            try {


                String jsonString = MainActivity.sharedPreferences.getString("jsonTestResults", "");

                jsonObject = new JSONObject(jsonString);

                jsonArray = jsonObject.getJSONArray("testResults");


                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject object = jsonArray.getJSONObject(i);

                    ResultsRecyclerViewItem resultsRecyclerViewItem = new ResultsRecyclerViewItem();

                    resultsRecyclerViewItem.setPercentage(object.getString("percentage"));
                    resultsRecyclerViewItem.setDay(object.getString("day"));
                    resultsRecyclerViewItem.setDate(object.getString("date"));
                    resultsRecyclerViewItem.setTime(object.getString("time"));
                    resultsRecyclerViewItem.setPassOrFail(object.getString("passOrFail"));
                    resultsRecyclerViewItem.setTestType(object.getString("testType"));

                    //time to complete test
                    //======================================================
                    int secondsLeft = Integer.parseInt(object.getString("totalTestTime")) - Integer.parseInt(object.getString("secondsRemaining"));
                    int minutes = (int) secondsLeft/60;
                    int seconds = secondsLeft - minutes * 60;

                    String secondString =   Integer.toString(seconds);

                    if(seconds < 10){
                        secondString = "0" + secondString;
                    }


                    resultsRecyclerViewItem.setTimeToCompleteTest(Integer.toString(minutes) + ":" + secondString);
                    //======================================================
                    //time to complete test




                    recyclerViewArrayList.add(resultsRecyclerViewItem);


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else{

            ResultsRecyclerViewItem resultsRecyclerViewItem = new ResultsRecyclerViewItem();
            resultsRecyclerViewItem.setTestType("There are no results available.");

            recyclerViewArrayList.add(resultsRecyclerViewItem);


        }







    }




    public void deleteResults(View view){

        if(MainActivity.sharedPreferences.getString("jsonTestResults", "").equals("")){

            Toast.makeText(ResultsRecyclerViewActivity.this,"There are no results to delete.",Toast.LENGTH_SHORT).show();

        }else {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Delete");
        alertDialog.setMessage("Are you sure?");



        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                            MainActivity.sharedPreferences.edit().putString("jsonTestResults", "").apply();

                            Toast.makeText(ResultsRecyclerViewActivity.this,"Results deleted.",Toast.LENGTH_SHORT).show();

                            recyclerViewArrayList.clear();

                            resultsRecyclerViewAdapter.notifyDataSetChanged();



                    }
                });



        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });



        alertDialog.show();

    }








    }







    @Override
    protected void onResume() {
        super.onResume();

        resultsRecyclerViewAdapter = new ResultsRecyclerViewAdapter(recyclerViewArrayList,this);

        recyclerView.setAdapter(resultsRecyclerViewAdapter);


    }



}
