package com.thetaverde.niall.cartheorytestireland;

import java.io.Serializable;

public class QuestionObject implements Serializable {


    private String Section;
    private String Question;
    private String CorrectAnswer;
    private String WrongAnswerOne;
    private String WrongAnswerTwo;
    private String WrongAnswerThree;

    private Boolean questionAnsweredCorrectly;
    private String wrongAnswerThatWasSelected;


    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public String getWrongAnswerOne() {
        return WrongAnswerOne;
    }

    public void setWrongAnswerOne(String wrongAnswerOne) {
        WrongAnswerOne = wrongAnswerOne;
    }

    public String getWrongAnswerTwo() {
        return WrongAnswerTwo;
    }

    public void setWrongAnswerTwo(String wrongAnswerTwo) {
        WrongAnswerTwo = wrongAnswerTwo;
    }

    public String getWrongAnswerThree() {
        return WrongAnswerThree;
    }

    public void setWrongAnswerThree(String wrongAnswerThree) {
        WrongAnswerThree = wrongAnswerThree;
    }

    public Boolean getQuestionAnsweredCorrectly() {
        return questionAnsweredCorrectly;
    }

    public void setQuestionAnsweredCorrectly(Boolean questionAnsweredCorrectly) {
        this.questionAnsweredCorrectly = questionAnsweredCorrectly;
    }

    public String getWrongAnswerThatWasSelected() {
        return wrongAnswerThatWasSelected;
    }

    public void setWrongAnswerThatWasSelected(String wrongAnswerThatWasSelected) {
        this.wrongAnswerThatWasSelected = wrongAnswerThatWasSelected;
    }
}
