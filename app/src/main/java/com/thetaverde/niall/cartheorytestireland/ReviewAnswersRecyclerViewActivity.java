package com.thetaverde.niall.cartheorytestireland;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;

public class ReviewAnswersRecyclerViewActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ReviewAnswersRecyclerViewAdapter reviewAnswersRecyclerViewAdapter;
    RecyclerView.LayoutManager layoutManager;



    ArrayList<QuestionObject> desiredQuestionObjects;

    ArrayList<ReviewAnswersRecyclerViewItem> recyclerViewArrayList;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.menu.question_activity_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home: // this is for the back arrow on the action bar
                finish();
                return true;

            default:
                return false;
        }

    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_answers_recycler_view);

        setTitle("Review Answers");

        //for the back button on the action bar
        if(getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }

        recyclerView =  findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        recyclerViewArrayList = new ArrayList<>();


        //getting the questionObjects that where asked during the theory test
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("questionObjectsArrayListBundle");
        desiredQuestionObjects = (ArrayList<QuestionObject>) args.getSerializable("questionObjectsArrayList");

        //populating the recyclerViewArrayList with the "ReviewAnswersRecyclerViewItem" objects
        for (int i = 0 ; i < desiredQuestionObjects.size(); i++){

            recyclerViewArrayList.add(new ReviewAnswersRecyclerViewItem(desiredQuestionObjects.get(i)));

        }







        reviewAnswersRecyclerViewAdapter = new ReviewAnswersRecyclerViewAdapter(recyclerViewArrayList,this);

        recyclerView.setAdapter(reviewAnswersRecyclerViewAdapter);


    }


    @Override
    protected void onResume() {
        super.onResume();

        reviewAnswersRecyclerViewAdapter = new ReviewAnswersRecyclerViewAdapter(recyclerViewArrayList,this);

        recyclerView.setAdapter(reviewAnswersRecyclerViewAdapter);


    }














}
