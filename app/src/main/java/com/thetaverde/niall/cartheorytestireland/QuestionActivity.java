package com.thetaverde.niall.cartheorytestireland;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class QuestionActivity extends AppCompatActivity {

    TextView timeTV;
    TextView questionTV;
    TextView questionCounterTV;

    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button quitButton;
    Button resetButton;

    long timeInSeconds;
    Integer numberOfQuestions;
    Integer numberOfQuestionsAnswered;
    Integer numberOfCorrectAnswers;

    String sectionName;

    String testType; //either a fullTest or a QuickTest

    long secondsRemaining;

    ArrayList<QuestionObject> desiredQuestionObjects;

    String greyButtonColourHexCode;

    Vibrator vibrator;

    CountDownTimer countDownTimer;

    int numberOfButtons = 4;

    ArrayList<Integer>  buttonIds;

    //defining the array that the will store the answers that will be assigned to a button
    String[] answers = new String[numberOfButtons];




    int correctAnswerLocation;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.question_activity_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case R.id.settings:
                Intent intent = new Intent(this, AppSettingsActivity.class);
                startActivity(intent);
                return true;

            case android.R.id.home: // this is for the back arrow on the action bar
                stopTimer();
                finish();
                return true;

            default:
                return false;
        }

    }







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        greyButtonColourHexCode = "#000000";


        timeTV = findViewById(R.id.timeTV);
        questionTV = findViewById(R.id.questionTV);
        questionCounterTV = findViewById(R.id.questionCounterTV);

        button0 = findViewById(R.id.button0);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);

        //initializing the buttonIds arraylist
        buttonIds = new ArrayList<>();

        buttonIds.add(button0.getId());
        buttonIds.add(button1.getId());
        buttonIds.add(button2.getId());
        buttonIds.add(button3.getId());



        quitButton = findViewById(R.id.quitButton);
        resetButton = findViewById(R.id.resetButton);

        //for the back button on the action bar
        if(getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }

        //initializing the vibrator
        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);


        Intent intent = getIntent();
        numberOfQuestions = Integer.valueOf(intent.getStringExtra("numberOfQuestions"));
        timeInSeconds = Long.valueOf(intent.getStringExtra("time"));
        testType = intent.getStringExtra("testType");
        sectionName = intent.getStringExtra("section");


        //setting the activity title depending on the type of test
        if(testType.equals("fullTest")){
            setTitle("Full Test");
        }else if (testType.equals("quickTest")){
            setTitle("Quick Test");
        }else{
            setTitle(sectionName);
        }






        //first index is the question
        //second index is the correct answer
        // the rest are the wrong answers.
        //getting the correct number of questionObjects for the local database
        desiredQuestionObjects = pullQuestionsFromLocalDatabase(numberOfQuestions, sectionName);

        Log.i("SizeOfQuestionArray", String.valueOf(desiredQuestionObjects.size()));





       //numberOfQuestions = 50;
       numberOfQuestionsAnswered = 1;

       numberOfCorrectAnswers = 0;




       resetQuestionActivity();


       newQuestion();



        //changing the test of the timerTV to "Practice" when it is not a full test or a quick test
        if (!testType.equals("fullTest") || !testType.equals("quickTest")){
            timeTV.setText("Practice");
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        stopTimer();

        Log.i("Back Pressed", "Yes");
    }



    @Override
    protected void onStop() {
        super.onStop();


        Log.i("OnStop", "Yes");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i(" OnDestroy","Yes");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("OnRestart","yes");
    }

    @Override
    protected void onResume() {
        super.onResume();



        Log.i("OnResume","yes");
    }



    public void newQuestion(){


        enableButtons();

        // i am using "numberOfQuestionsAnswered - 1" because the first questionObject is at index "0" in the arraylist
        questionTV.setText(desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).getQuestion());

        Log.i("QuestionOne",desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).getQuestion());


        Set<Integer> randomNumbers = new HashSet<>();
        ArrayList<Integer> randomNumberAL = new ArrayList<>();




        Random random = new Random();

        //random.nextInt generates a number between 0 (inclusive) and n (exclusive)
        //here i generate a number between 0 and 3
        correctAnswerLocation = random.nextInt(numberOfButtons);


        //i am using the set to make sure the same random number isn't generated twice.
        //however the set orders the numbers in ascending order so i am  using an arraylist to store the order the random numbers are generated

        randomNumbers.add(correctAnswerLocation);
        randomNumberAL.add(correctAnswerLocation);

        Log.i("CorrectAnswerLocation", String.valueOf(correctAnswerLocation));

        int numberOfRandomNumbersRequired = 3;
        for (int j = 0; j < numberOfRandomNumbersRequired; j++ ) {

            int incorrectAnswerLocation = random.nextInt(numberOfButtons );

            while (randomNumbers.contains(incorrectAnswerLocation)) {

                incorrectAnswerLocation = random.nextInt(numberOfButtons );

                Log.i("Here", String.valueOf(incorrectAnswerLocation));

            }

            randomNumbers.add(incorrectAnswerLocation);
            randomNumberAL.add(incorrectAnswerLocation);
        }




        answers[randomNumberAL.get(0)] = desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).getCorrectAnswer(); //correct answer
        answers[randomNumberAL.get(1)] = desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).getWrongAnswerOne(); //incorrect answer
        answers[randomNumberAL.get(2)] = desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).getWrongAnswerTwo(); //incorrect answer
        answers[randomNumberAL.get(3)] = desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).getWrongAnswerThree(); //incorrect answer



        button0.setText(answers[0]);
        button1.setText(answers[1]);
        button2.setText(answers[2]);
        button3.setText(answers[3]);





    }




    public void chosenAnswer(View view){


        //disabling the onclick for the buttons when a button is click so that they can't be hit more than once
        disableButtons();

        //ensuring that the button background colour is grey
        setButtonBackGroundToGrey();

        //if the coloured (correct/incorrect) buttons feature is turned off them this will be used instead
        Button pressedButton = findViewById(view.getId());
        pressedButton.setBackgroundColor(Color.GRAY);



        if (view.getTag().toString().equals(String.valueOf(correctAnswerLocation)) ){



            numberOfCorrectAnswers = numberOfCorrectAnswers + 1;

            desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).setQuestionAnsweredCorrectly(true);


            //if the user has turned on coloured buttons in the setting then execute this code - default is true
            if(Boolean.valueOf(MainActivity.sharedPreferences.getString("colouredButtonsSettingsSwitch", "false"))) {
                Button correctButton = findViewById(buttonIds.get(correctAnswerLocation));
                correctButton.setBackgroundColor(Color.GREEN);
            }




        }else{



            Button correctButton = findViewById(buttonIds.get(correctAnswerLocation));
            Button incorrectButton = findViewById(view.getId());



            //storing a boolean that tells you that the answer is wrong and storing the wrong answer that the use entered
            desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).setQuestionAnsweredCorrectly(false);
            desiredQuestionObjects.get(numberOfQuestionsAnswered - 1).setWrongAnswerThatWasSelected(incorrectButton.getText().toString());





            //if the user has turned on coloured buttons in the setting then execute this code - default is true
            if(Boolean.valueOf(MainActivity.sharedPreferences.getString("colouredButtonsSettingsSwitch", "false"))) {
                correctButton.setBackgroundColor(Color.GREEN);
                incorrectButton.setBackgroundColor(Color.RED);
            }


            //if the user has turned on vibration in the setting then execute this code - default is true
            if(Boolean.valueOf(MainActivity.sharedPreferences.getString("vibrateSettingSwitch", "false"))) {
                // Vibrate for 500 milliseconds
                vibrator.vibrate(100);
            }



        }












        new CountDownTimer(200, 200) {
            public void onFinish() {
                // When timer is finished
                // Execute your code here

                setButtonBackGroundToGrey();

                //if the number of question answered exceeds the number of questions available then the test is over
                //otherwise, show the next question.
                if(numberOfQuestionsAnswered >= numberOfQuestions){

                    testIsOver("false");

                }else {

                    //updating the question counter
                    numberOfQuestionsAnswered = numberOfQuestionsAnswered + 1;

                    questionCounterTV.setText("Q" + String.valueOf(numberOfQuestionsAnswered) + " of " + String.valueOf(numberOfQuestions));

                    newQuestion();
                }

            }

            public void onTick(long millisUntilFinished) {
                // millisUntilFinished    The amount of time until finished.
            }
        }.start();






    }






    public ArrayList<QuestionObject> pullQuestionsFromLocalDatabase(int numberOfQuestionsDesired, String sectionName){


        DatabaseHelper databaseHelper = new DatabaseHelper(this);

        ArrayList<QuestionObject> allQuestions = databaseHelper.getQuestions(sectionName);

        ArrayList<QuestionObject> shortenedListOfQuestionObjects = new ArrayList<>();



        //ensuring that there are more question available than the number of question that is being requested.
        if(numberOfQuestionsDesired > allQuestions.size()) {

            numberOfQuestionsDesired = allQuestions.size();

            numberOfQuestions = allQuestions.size();

        }






        Random random = new Random();

        Set<Integer> randomNumberSet = new HashSet<>();

        for (int j = 0; j < numberOfQuestionsDesired; j++ ) {

            int currentRandomNumber = random.nextInt(allQuestions.size());

            while (randomNumberSet.contains(currentRandomNumber)) {

                currentRandomNumber = random.nextInt(allQuestions.size());

            }


            randomNumberSet.add(currentRandomNumber);


        }

            ArrayList<Integer> newList = new ArrayList<>(randomNumberSet);



            for (int h = 0; h < numberOfQuestionsDesired; h++){

                shortenedListOfQuestionObjects.add(allQuestions.get(newList.get(h)));

                //Log.i("RandomNumbers", String.valueOf(h) + ": " + String.valueOf(newList.get(h)));

                Log.i("ShortedListOfQuestion", String.valueOf(h) + ": " + allQuestions.get(newList.get(h)).getQuestion());

            }







        return shortenedListOfQuestionObjects;

    }






    //method executed when the timer runs out or if the user finishes the last question
    public void testIsOver(String quitBoolean){


        stopTimer();

        Intent intent =  new Intent(QuestionActivity.this, TestOverActivity.class );
        intent.putExtra("numberOfQuestions", String.valueOf(numberOfQuestions));
        intent.putExtra("numberOfCorrectAnswers",String.valueOf(numberOfCorrectAnswers));
        intent.putExtra("time",String.valueOf(timeInSeconds));
        intent.putExtra("testType",testType);
        intent.putExtra("secondsRemaining",String.valueOf(secondsRemaining));
        intent.putExtra("section", sectionName);
        intent.putExtra("quitBoolean",quitBoolean);

        Bundle bundle = new Bundle();

        bundle.putSerializable("questionObjectsArrayList",(Serializable) desiredQuestionObjects);

        intent.putExtra("questionObjectsArrayListBundle", bundle);

        startActivity(intent);

        finish();


    }























    //Quiting the activity
    public void quit(View view){

       // Intent intent = new Intent(this, MainActivity.class);
        //startActivity(intent);

        testIsOver("true");

    }






    //Countdown timer
    //=================================================
    public void startTimer(long time){

        stopTimer();

        //if it is a quick test or a full test then start the time
        //if it is a practise topic/section test then no timer is required
        if (testType.equals("fullTest") || testType.equals("quickTest")) {

            countDownTimer = new CountDownTimer(time * 1000 + 900, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    secondsRemaining = millisUntilFinished / 1000;

                    updateTimer((int) (millisUntilFinished / 1000));

                }

                @Override
                public void onFinish() {


                    timeTV.setText("0:00");

                    testIsOver("false");

                }
            }.start();

        }



    }





    public void updateTimer(int secondsLeft){

        //Log.i("In updateTimer"," Yes");

        int minutes = (int) secondsLeft/60;
        int seconds = secondsLeft - minutes * 60;

        String secondString =   Integer.toString(seconds);

        if(seconds < 10){
            secondString = "0" + secondString;
        }

        timeTV.setText(Integer.toString(minutes) + ":" + secondString);



    }


    public void resetTimer(long timeInSeconds){

        int minutes = (int) timeInSeconds/60;
        int seconds = (int) (timeInSeconds - minutes * 60);

        stopTimer();

        timeTV.setText( String.valueOf(minutes) + ":" + String.valueOf(seconds));

        startTimer(timeInSeconds);

    }

    public void stopTimer(){

        if(countDownTimer != null) {
            countDownTimer.cancel();
        }

    }




    public void resetQuestionActivity(){

        numberOfQuestionsAnswered = 1;

        numberOfCorrectAnswers = 0;

        //first index is the question
        //second index is the correct answer
        // the rest are the wrong answers.
        //getting the correct number of questionObjects for the local database
        desiredQuestionObjects = pullQuestionsFromLocalDatabase(numberOfQuestions, sectionName);


        questionCounterTV.setText("Q" + String.valueOf(numberOfQuestionsAnswered) + " of " + String.valueOf(numberOfQuestions));


        setButtonBackGroundToGrey();

        resetTimer(timeInSeconds);



        newQuestion();


    }

    //resetting the timer and test
    public void reset(View view){

        resetQuestionActivity();


    }





    public void setButtonBackGroundToGrey(){

        button0.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));
        button1.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));
        button2.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));
        button3.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));
    }



    public void enableButtons(){

        button0.setClickable(true);
        button1.setClickable(true);
        button2.setClickable(true);
        button3.setClickable(true);

    }

    public void disableButtons(){

        button0.setClickable(false);
        button1.setClickable(false);
        button2.setClickable(false);
        button3.setClickable(false);

    }






}
