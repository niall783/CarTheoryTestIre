package com.thetaverde.niall.cartheorytestireland;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TestOverActivity extends AppCompatActivity {


    TextView passOrFailTV;
    TextView percentageCorrectAnswersTV;
    TextView correctAnswersResultTV;


    Button mainMenuButton;
    Button reviewQuestionsButton;
    Button tryAgainButton;


    String greyButtonColourHexCode;

    int  numberOfQuestions;
    int numberOfCorrectAnswers;

    int timeInSeconds;

    int secondsRemaining;

    String testType; //either a full test or a quick test

    String sectionName;

    Boolean quitBoolean;

    ArrayList<QuestionObject> desiredQuestionObjects;

    //button0.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.menu.question_activity_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home: // this is for the back arrow on the action bar
                finish();
                return true;

            default:
                return false;
        }

    }









    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_over);

        setTitle("Test is over!");


        //for the back button on the action bar
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        passOrFailTV = findViewById(R.id.passOrFailTV);
        percentageCorrectAnswersTV = findViewById(R.id.percentageCorrectAnswersTV);
        correctAnswersResultTV = findViewById(R.id.correctAnswersResultTV);


        mainMenuButton = findViewById(R.id.mainMenu);
        tryAgainButton = findViewById(R.id.tryAgain);
        reviewQuestionsButton = findViewById(R.id.reviewQuestions);


        greyButtonColourHexCode = "#000000";

        mainMenuButton.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));
        tryAgainButton.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));
        reviewQuestionsButton.setBackgroundColor(Color.parseColor(greyButtonColourHexCode));


        Intent intent = getIntent();
        timeInSeconds = Integer.parseInt(intent.getStringExtra("time"));

        Bundle args = intent.getBundleExtra("questionObjectsArrayListBundle");

        desiredQuestionObjects = (ArrayList<QuestionObject>) args.getSerializable("questionObjectsArrayList");



        numberOfCorrectAnswers = Integer.parseInt(intent.getStringExtra("numberOfCorrectAnswers"));
        numberOfQuestions = Integer.parseInt(intent.getStringExtra("numberOfQuestions"));
        testType = intent.getStringExtra("testType");
        secondsRemaining = Integer.parseInt(intent.getStringExtra("secondsRemaining"));

        sectionName = intent.getStringExtra("section");

        quitBoolean = Boolean.valueOf(intent.getStringExtra("quitBoolean"));



        //this prints out the percentage of correct answers
        DecimalFormat decimalFormat = new DecimalFormat("###.#");
        Double percentageInTest = ((double) numberOfCorrectAnswers / (double) numberOfQuestions)*100;
        percentageCorrectAnswersTV.setText(String.valueOf(decimalFormat.format(percentageInTest)) + "%");



        //this prints out the number of correct answers in the following format "you got 3 out of 10"
        DecimalFormat df = new DecimalFormat("###");
        correctAnswersResultTV.setText("You got "  + String.valueOf(df.format(numberOfCorrectAnswers)) + " out of " + String.valueOf(df.format(numberOfQuestions)));




        Double testPassPercentage = 87.5;

        //this changes from "pass" or "fail depending the the result achieved"
        if(percentageInTest >= testPassPercentage){
            passOrFailTV.setText("Pass");
            passOrFailTV.setTextColor(Color.GREEN);

        }else {
            passOrFailTV.setText("Fail");
            passOrFailTV.setTextColor(Color.RED);
        }



        //saving the result to shared preferences in as a jsonArray
        saveResults();


    }




    public void mainMenu(View view){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        finish();
    }


    public void tryAgain(View view){

        Intent intent = new Intent(this,QuestionActivity.class);
        intent.putExtra("numberOfQuestions",String.valueOf(numberOfQuestions));
        intent.putExtra("time",String.valueOf(timeInSeconds));
        intent.putExtra("testType",testType);
        intent.putExtra("section",sectionName);
        startActivity(intent);
        finish();

    }



    public void reviewAnswers(View view){

        Intent intent = new Intent(this, ReviewAnswersRecyclerViewActivity.class);

        Bundle bundle = new Bundle();

        bundle.putSerializable("questionObjectsArrayList",(Serializable) desiredQuestionObjects);

        intent.putExtra("questionObjectsArrayListBundle", bundle);

        startActivity(intent);




    }


    public void saveResults(){

        Log.i("QuitBoolean", String.valueOf(quitBoolean));

        //if the user did not quit then save their results
        if(!quitBoolean) {


            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();

            JSONObject jsonItem = new JSONObject();


            Date currentTime = Calendar.getInstance().getTime();

            //("EEE, d MMM yyyy, HH:mm");

            DateFormat day = new SimpleDateFormat("EEE");
            DateFormat date = new SimpleDateFormat("d MMM yyyy");
            DateFormat time = new SimpleDateFormat("HH:mm");

            String dayString = day.format(currentTime);
            String dateString = date.format(currentTime);
            String timeString = time.format(currentTime);


            Log.i("Day", dayString);
            Log.i("Date", dateString);
            Log.i("Time", timeString);


            try {

                //extracting the json Objects from the shared preferences json string
                if (!MainActivity.sharedPreferences.getString("jsonTestResults", "").equals("")) {

                    Log.i("Shared Preferences", "Yes");

                    String jsonString = MainActivity.sharedPreferences.getString("jsonTestResults", "");

                    jsonObject = new JSONObject(jsonString);

                    jsonArray = jsonObject.getJSONArray("testResults");


                    jsonItem.put("percentage", percentageCorrectAnswersTV.getText().toString());
                    jsonItem.put("numberOfQuestions", String.valueOf(numberOfQuestions));
                    jsonItem.put("numberOfCorrectAnswers", String.valueOf(numberOfCorrectAnswers));
                    jsonItem.put("passOrFail", passOrFailTV.getText().toString());
                    jsonItem.put("testType", testType);
                    jsonItem.put("totalTestTime", String.valueOf(timeInSeconds));
                    jsonItem.put("secondsRemaining", String.valueOf(secondsRemaining));
                    jsonItem.put("day", dayString); //current time
                    jsonItem.put("date", dateString); //current date
                    jsonItem.put("time", timeString); //current time


                    jsonArray.put(jsonItem);

                    jsonObject.put("testResults", jsonArray);

                    MainActivity.sharedPreferences.edit().putString("jsonTestResults", jsonObject.toString()).apply();


                    Log.i("testResults", jsonObject.toString());


                } else {


                    jsonItem.put("percentage", percentageCorrectAnswersTV.getText().toString());
                    jsonItem.put("numberOfQuestions", String.valueOf(numberOfQuestions));
                    jsonItem.put("numberOfCorrectAnswers", String.valueOf(numberOfCorrectAnswers));
                    jsonItem.put("passOrFail", passOrFailTV.getText().toString());
                    jsonItem.put("testType", testType);
                    jsonItem.put("totalTestTime", String.valueOf(timeInSeconds));
                    jsonItem.put("secondsRemaining", String.valueOf(secondsRemaining));
                    jsonItem.put("day", dayString); //current day
                    jsonItem.put("date", dateString); //current date
                    jsonItem.put("time", timeString); //current time


                    jsonArray.put(jsonItem);

                    jsonObject.put("testResults", jsonArray);

                    MainActivity.sharedPreferences.edit().putString("jsonTestResults", jsonObject.toString()).apply();


                    Log.i("testResults", jsonObject.toString());


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }



    }






















}
