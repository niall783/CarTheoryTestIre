package com.thetaverde.niall.cartheorytestireland;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    static SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Theory Test");

        sharedPreferences = MainActivity.this.getSharedPreferences("com.example.niall.theorytest", Context.MODE_PRIVATE);



        DatabaseHelper databaseHelper = new DatabaseHelper(this);

        databaseHelper.createDatabase();

        ArrayList<String> sectionNames = databaseHelper.getSectionNames();

        for (int i = 0; i < sectionNames.size(); i++) {
            Log.i("SectionName",sectionNames.get(i));

        }




    }



    public void fullTest(View view){


        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra("numberOfQuestions","40");
        intent.putExtra("time",String.valueOf(45*60)); //time in seconds
        intent.putExtra("testType","fullTest");
        intent.putExtra("section","");
        startActivity(intent);


    }

    public void quickTest(View view){



        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra("numberOfQuestions","10");
        intent.putExtra("time",String.valueOf(5*60)); //time in seconds
        intent.putExtra("testType","quickTest");
        intent.putExtra("section","");
        startActivity(intent);



    }

    public void practise(View view){

        Intent intent = new Intent(this, SectionsRecyclerViewActivity.class);
        startActivity(intent);


    }


    public void results(View view){

        Intent intent = new Intent(this, ResultsRecyclerViewActivity.class);
        startActivity(intent);


    }


}

