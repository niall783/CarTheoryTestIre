package com.thetaverde.niall.cartheorytestireland;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

public class AppSettingsActivity extends AppCompatActivity {


    Switch colouredButtonsSettingsSwitch;
    Switch vibrateSettingSwitch;

    Boolean colouredButtonsSettingsSwitchState;
    Boolean vibrateSettingSwitchState;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        //menuInflater.inflate(R.menu.question_activity_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home: // this is for the back arrow on the action bar
                finish();
                return true;

            default:
                return false;
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);

        setTitle("Settings");


        if(getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }


        colouredButtonsSettingsSwitch = findViewById(R.id.colouredButtonsSettingsSwitch);
        vibrateSettingSwitch = findViewById(R.id.vibrateSettingSwitch);


        //getting the settings from shared preferences
        colouredButtonsSettingsSwitchState = Boolean.valueOf(MainActivity.sharedPreferences.getString("colouredButtonsSettingsSwitch", "false"));
        vibrateSettingSwitchState = Boolean.valueOf(MainActivity.sharedPreferences.getString("vibrateSettingSwitch", "false"));


        //setting the state of the switches to the state that is saves in the shared preferences
        vibrateSettingSwitch.setChecked(vibrateSettingSwitchState);

        colouredButtonsSettingsSwitch.setChecked(colouredButtonsSettingsSwitchState);





        Log.i("Switch (Vibrate)", String.valueOf(vibrateSettingSwitchState));

        Log.i("Switch (Coloured)", String.valueOf(colouredButtonsSettingsSwitchState));



        colouredButtonsSettingsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                Log.i("Switch (Coloured)", String.valueOf(isChecked));

                MainActivity.sharedPreferences.edit().putString("colouredButtonsSettingsSwitch", String.valueOf(isChecked)).apply();

            }
        });


        vibrateSettingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                MainActivity.sharedPreferences.edit().putString("vibrateSettingSwitch", String.valueOf(isChecked)).apply();


                Log.i("Switch (Vibrate)", String.valueOf(isChecked ));

            }
        });



    }
}
