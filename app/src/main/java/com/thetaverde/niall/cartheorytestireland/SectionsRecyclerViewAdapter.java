package com.thetaverde.niall.cartheorytestireland;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class SectionsRecyclerViewAdapter extends RecyclerView.Adapter<SectionsRecyclerViewAdapter.MyViewHolder> {




    ArrayList<SectionsRecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();

    Context context;


    SectionsRecyclerViewAdapter(ArrayList<SectionsRecyclerViewItem> recyclerViewItemsArrayList, Context context){

        this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;

        this.context = context;

    }


    @Override
    public SectionsRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sections_recycler_view_row,parent,false);

        return new SectionsRecyclerViewAdapter.MyViewHolder(view,context,recyclerViewItemsArrayList);
    }

    @Override
    public void onBindViewHolder(final SectionsRecyclerViewAdapter.MyViewHolder holder, int position) {


        String sectionString = "Section " + String.valueOf(position + 1) + ": " + recyclerViewItemsArrayList.get(position).getSectionName();

        holder.sectionNameRV_row.setText(sectionString);


        //holder.caveName.setText(recyclerViewItemsArrayList.get(position.getCaveName());
        //holder.caveCountry.setText(recyclerViewItemsArrayList.get(position).getCaveCountry());

    }

    @Override
    public int getItemCount() {
        return recyclerViewItemsArrayList.size();
    }





    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        //TextView caveName;
        //TextView caveCountry;

        TextView sectionNameRV_row;

        ArrayList<SectionsRecyclerViewItem> recyclerViewItemsArrayList = new ArrayList<>();
        Context context;


        public MyViewHolder(View itemView, Context context, ArrayList<SectionsRecyclerViewItem> recyclerViewItemsArrayList) {
            super(itemView);

            itemView.setOnClickListener(this);

            this.recyclerViewItemsArrayList = recyclerViewItemsArrayList;
            this.context = context;

            //caveName = (TextView) itemView.findViewById(R.id.caveNameRecyclerViewTextView);
            //caveCountry = (TextView) itemView.findViewById(R.id.caveCountryRecyclerViewTextView);



            sectionNameRV_row = itemView.findViewById(R.id.sectionNameRV_row);

        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();

            SectionsRecyclerViewItem recyclerViewItem = this.recyclerViewItemsArrayList.get(position);


            Intent intent = new Intent(context, QuestionActivity.class);
            intent.putExtra("numberOfQuestions","10");
            intent.putExtra("time",String.valueOf(1*60)); //time in seconds
            intent.putExtra("testType",recyclerViewItem.getSectionName());
            intent.putExtra("section",recyclerViewItem.getSectionName());
            context.startActivity(intent);




        }
    }







}
